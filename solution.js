// Submitter: Sagi Chubok

students = ["Eden", "Refael", "Yoni", "Nitzan", "Hadas"];

attendees = [
  ["Eden", "Refael", "Yoni", "Nitzan", "Hadas", "Ortal"],
  ["Berry", "Nitzan", "Yoni", "Eden", "Hadas", "Ortal"],
  ["Maxim", "Ortal", "Yoni", "Refael", "Nitzan", "Alex"],
  ["Eden", "Andrew", "Yoni", "Nitzan", "Ortal", "Nitzan"],
];

N = 3;

const topNStudentsAttendees = (students, attendees, N) => {
  const dict = Object.assign({}, ...students.map((student) => ({[student]: 0})));

  // Remove duplicates from attendees (accidentally registered again)
  for (let i = 0; i < attendees.length; i++) {
    attendees[i] = [...new Set(attendees[i])];
  }

  // Merge for count registration
  const merged = [].concat.apply([], attendees);
  // Set registration count for each student
  for (let student in dict) {
    dict[student] = merged.filter(attendee => attendee === student).length;
  }

  // Create items array for sorting
  let items = Object.keys(dict).map((key) => [key, dict[key]]);
  // Sort the array based on the registration count
  items.sort((first, second) => second[1] - first[1]);
  
  // Create a new array with only the first N items
  items = items.slice(0, N);

  return [].concat.apply([], items.map((item) => item[0]));
}

console.log(topNStudentsAttendees(students, attendees, N));
